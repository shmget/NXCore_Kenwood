## NXCore_Kenwood  [![CircleCI](https://circleci.com/gh/rthoelen/NXCore_Kenwood.svg?style=shield)](https://circleci.com/gh/rthoelen/NXCore_Kenwood)

# NXCore Manager for Kenwood Repeaters

To build with Debian, Raspbian, Ubuntu:

Run the build_debian.sh script.  This will install dependencies and generate the binary.
